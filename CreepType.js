"use strict";

class CreepType {
	constructor(minCreeps, bodyParts, killTtl = 200) {
		this.activeCount = 0;
		this.cost = 0;
		this.minCreeps = minCreeps;
		this.bodyParts = bodyParts;
		this.killTtl = killTtl;
		for (let part of this.bodyParts) {
			this.cost += BODYPART_COST[part];
		}
	}
	getEnergyCapacity() {
		let capacity = 0;
		for (let part of this.bodyParts) {
			if (part === CARRY) {
				capacity += 50;
			}
		}
		return capacity;
	}
	incrementActiveCount() {
		this.activeCount++;
	}
	getDesiredCount() {
		return this.minCreeps;
	}
	getCost() {
		return this.cost;
	}
	static getCreepType(creep) {
		if (creep.memory['creepType'] === undefined) {
			creep.memory['creepType'] = 'TypeBunny';
		}
		return creep.memory['creepType'];
	}
	createCreep(room) {
		let spawn = room.find(FIND_MY_SPAWNS)[0];
		let success = spawn.spawnCreep(this.bodyParts, this.getType() + Game.time.toString(), { memory: { creepType: this.getType() } });
		if (success === 0) {
			this.activeCount++;
			console.log("Added", this.getType(), "Creep in", room.name);
		}
		else if (success !== ERR_BUSY) {
			console.log("Error adding creep \"", this.getType(), "\":", success);
		}
		this.freeCreepMemory();
		return true;
	}
	freeCreepMemory() {
		for (let creepName in Memory.creeps) {
			if (!Game.creeps.hasOwnProperty(creepName)) {
				delete Memory.creeps[creepName];
			}
		}
	}
	setActiveCount(activeCount) {
		this.activeCount = activeCount;
	}
	getActiveCount() {
		return this.activeCount;
	}
}
// noinspection JSUnresolvedVariable
module.exports = CreepType;

