"use strict";
const _ = require('lodash');

const CreepType = require("CreepType");
const CreepAction = require("CreepAction");
const EnergySources = require("EnergySources");
const EnergyStorage = require("EnergyStorage");
const levels = {
	L8: 12900, // 3 * 300 + 60 * 200
	L7:  5600, // 2 * 300 + 50 * 100,
	L6:  2300, // 1 * 300 + 40 * 50,
	L5:  1800, // 1 * 300 + 30 * 50,
	L4:  1300, // 1 * 300 + 20 * 50,
	L3:   800, // 1 * 300 + 10 * 50,
	L2:   550, // 1 * 300 + 5 * 50,
	L1:   300, //1 * 300
};

class MyRoom {
	constructor(roomName, config) {
		this.level = null;
		this.emergencyRecovery = true;
		this.name = roomName;
		this.config = config;
		this.energySources = null;
		this.energyStorage = null;
		this.tickCount = 999999999;
	}
	getRoom() {
		return Game.rooms[this.name];
	}
	setLevel(level) {
		let roomLevels = this.config.getRoomLevels(this.name);
		if (level !== this.level) {
			this.level = level;
			console.log("Assigned level", level, "-", levels[level], "Energy");
		}
		this.creepTypes = roomLevels.get(level);
	}
	setup() {
		/* @var {Room} room */
		let room = Game.rooms[this.name];
		this.creepTypes = null;
		let roomLevels = this.config.getRoomLevels(this.name);
		let creepCount = room.find(FIND_MY_CREEPS, {filter: c => CreepType.getCreepType(c) === 'TypeBunny'}).length;
		if (creepCount <= 2) {
			this.setLevel('L1');
			this.emergencyRecovery = true;
		}
		else {
			this.emergencyRecovery = false;
			for (let level in levels) {
				let expectedEnergy = levels[level];
				if (expectedEnergy <= room.energyCapacityAvailable && roomLevels.has(level)) {
					this.setLevel(level);
					break;
				}
			}
		}
	}
	getEnergyItems(creep, forceSource = false) {
		let item = null;
		if (this.energySources.isLeeching(creep)) {
			if (this.energySources.isLeechingFromMe(creep)) {
				item = this.energySources;
			}
			else if (this.energySources.isLeechingFromMe(creep)) {
				item = this.energyStorage;
			}
		}
		if (item === null) {
			if (this.energySources.isLeeching(creep)) {
				this.energySources.stopLeach(creep, true);
			}
			if (forceSource || !this.energyStorage.hasStorage()) {
				item = this.energySources;
			}
			else {
				item = this.energyStorage;
			}
		}
		return item;
	}
	getCreepAction(creep) {
		let creepType = this.creepTypes.get(CreepType.getCreepType(creep));
		let actions = this.config.actions.get(creepType.getType());
		let action;
		let actionName;
		if (creepType.prepareForAction(creep, this)) {
			actionName = CreepAction.getCreepActionName(creep);
			if (this.emergencyRecovery && creepType.getType() === 'TypeBunny') {
				if (actionName !== 'ActionSpawnPower') {
					actionName = 'ActionSpawnPower';
					action = actions.get(actionName);
					action.assignCreep(creep);
				}
			}
			else if (!actionName) {
				for (let name of actions.keys()) {
					action = actions.get(name);
					if (action.canAssign() && action.getAssignmentCount() < action.getMinCreeps()) {
						action.assignCreep(creep);
						actionName = action.getName();
						break;
					}
				}
				if (!actionName) {
					for (let name of actions.keys()) {
						action = actions.get(name);
						if (action.canAssign() && action.getAssignmentCount() < action.getDesiredCreepCount(creepType.getEnergyCapacity())) {
							action.assignCreep(creep);
							actionName = name;
							break;
						}
					}
				}
			}
			else {
				action = actions.get(actionName);

			}
		}
		return action;
	}
	getCreeps() {
		let roomCreeps = [];
		let creeps = this.getRoom().find(FIND_MY_CREEPS);
		for (let creep of creeps) {
			if (creep.room.name === this.name && creep.memory['ignore'] === undefined) {
				this.creepTypes.get(CreepType.getCreepType(creep)).incrementActiveCount();
				roomCreeps.push(creep);
				let actionName = CreepAction.getCreepActionName(creep);
				if (actionName !== undefined) {
					this.config.actions.get(CreepType.getCreepType(creep)).get(actionName).incrementAssignmentCount();
				}
			}
		}
		return roomCreeps;
	}
	run() {
		if (this.tickCount++ > 1000) {
			console.log("Rebuild initiated");
			this.energySources = new EnergySources(this.getRoom());
			this.energyStorage = new EnergyStorage(this.getRoom());
			this.setup();
			console.log("Created room assignment:", this.name, "-", this.creepTypes.size, "creep type(s) assigned");
			console.log("Rebuild complete");
			this.tickCount = 0;
		}
		this.sentinel();
		CreepAction.setRoom(this.getRoom());
		for (let creepTypeName of this.config.actions.keys()) {

			this.creepTypes.get(creepTypeName).setActiveCount(0);
			let actions = this.config.actions.get(creepTypeName);
			for (let actionName of actions.keys()) {
				actions.get(actionName).setup();
			}
		}
		let creeps = this.getCreeps();
		for (let creep of creeps) {
			let action = this.getCreepAction(creep);
			if (action) {
				//console.log(action.name, action.constructor.name);
				action.execute(creep);
			}
		}
		for (let creepTypeName of this.creepTypes.keys()) {
			let creepType = this.creepTypes.get(creepTypeName);
			if (creepType.getActiveCount() < creepType.getDesiredCount()) {
				if (this.getRoom().energyAvailable >= creepType.getCost()) {
					creepType.createCreep(this.getRoom());
				}
				break;
			}
		}
		CreepAction.cleanup();
	}
	sentinel() {
		for (let structure of this.getRoom().find(FIND_STRUCTURES, { filter: (s) => s.structureType === STRUCTURE_TOWER })) {
			let tower = structure;
			let hostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
			if (hostile) {
				console.log("Attack!");
				tower.attack(hostile);
			}
			else {
				let repair = tower.pos.findClosestByRange(FIND_STRUCTURES,
						{ filter: s => s.hits < s.hitsMax && s.hits < 10000 });
				if (repair) {
					tower.repair(repair);
				}
			}
		}
	}
}

// noinspection JSUnresolvedVariable
module.exports = MyRoom;
