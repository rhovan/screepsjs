"use strict";

const EnergyItems = require("EnergyItems");

class EnergyStorage extends EnergyItems {
	buildItems(room) {
		if (room.storage) {
			this.items.set(room.storage.id, 0);
		}
	}
	getEnergyCapacity(id) {
		return this.getItem(id).store.getUsedCapacity();
	}
	leechEnergy(item, creep) {
		return creep.withdraw(
			item,
			RESOURCE_ENERGY,
			Math.min(creep.store.getFreeCapacity(),
			item.store.getUsedCapacity()));
	}
}
// noinspection JSUnresolvedVariable
module.exports = EnergyStorage;
