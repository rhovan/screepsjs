"use strict";

const CreepAction = require('CreepAction');
const _ = require('lodash');

class ActionWallRepair extends CreepAction {
	constructor() {
		super();
		this.percentage = 0.00005;
		this.percentageIncrement = 0.00005;
		this.creepType = 'TypeBunny';
		this.action = '🚧 repair';

	}
	getStructures(room) {
		return room.find(FIND_STRUCTURES, { filter: (s) => (s.structureType === STRUCTURE_WALL ||
					s.structureType === STRUCTURE_RAMPART) });
	}
	getTargets(room) {
		let structures = this.getStructures(room);
		let roomObjects = [];
		if (structures.length > 0) {
			while (roomObjects.length === 0) {
				for (let structure of structures) {
					if (structure.hits / structure.hitsMax < this.percentage) {
						roomObjects.push(structure);
					}
				}
				if (roomObjects.length === 0) {
					this.percentage += this.percentageIncrement;
				}
			}
		}
		return _.sortBy(roomObjects, s => s.hits);
	}
	getRequiredEnergy(_targets) {
		return 0;
	}
	getDesiredCreepCount(_creepCapacity) {
		let length = this.getCachedTargets().length;
		return length > 0 ? 1 : 0;
	}
	execute(creep) {
		let wall = Game.getObjectById(creep.memory['targetId']);
		if (!wall || wall.hits / wall.hitsMax > this.percentage) {
			CreepAction.unassignCreep(creep);
		}
		else {
			if (creep.repair(wall) === ERR_NOT_IN_RANGE) {
				creep.moveTo(wall, { visualizePathStyle: { stroke: '#ffffff' } });
			}
		}
	}
}
// noinspection JSUnresolvedVariable
module.exports = ActionWallRepair;
