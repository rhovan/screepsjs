"use strict";
const CreepAction = require('CreepAction');

class ActionRoadRepair extends CreepAction {
	constructor() {
		super();
		this.creepType = 'TypeBunny';
		this.action = '🚧 repair';
		this.findOptions = {
			filter: (s) => (s.structureType === STRUCTURE_ROAD) && (s.hits < s.hitsMax)
		};
	}
	getRequiredEnergy(targets) {
		let energy = 0;
		for (let target of targets) {
			let structure = target;
			energy += (structure.hitsMax - structure.hits) / 10;
		}
		return energy;
	}
	execute(creep) {
		let structure = Game.getObjectById(creep.memory['targetId']);
		if (!structure || structure.hits === structure.hitsMax) {
			CreepAction.unassignCreep(creep);
		}
		else {
			if (creep.repair(structure) === ERR_NOT_IN_RANGE) {
				creep.moveTo(structure, { visualizePathStyle: { stroke: '#ffffff' } });
			}
		}
	}
}

// noinspection JSUnresolvedVariable
module.exports = ActionRoadRepair;
