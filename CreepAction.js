"use strict";
let _ = require('lodash');

class CreepAction {
	constructor() {
		this.creepType = null;
		this.assignedCount = 0;
		this.requiredEnergy = null;
		this.findOptions = null;
		this.findType = FIND_STRUCTURES;
		this.sort = null;
	}

	getTargets(room) {
		let items = room.find(this.findType, this.findOptions);
		if (items.length > 0 && this.sort !== null) {
			items = _.sortBy(items, this.sort);
		}
		return items;
	}

	static setRoom(room) {
		CreepAction.room = room;
	}
	getCreepType() {
		return this.creepType;
	}
	setup() {
		this.assignedCount = 0;
	}
	incrementAssignmentCount() {
		this.assignedCount++;
	}
	getAssignmentCount() {
		return this.assignedCount;
	}
	getCachedTargets() {
		if (!CreepAction.targets.has(this.getName())) {
			let targets = this.getTargets(CreepAction.room);
			if (!targets) {
				targets = [];
			}
			CreepAction.targets.set(this.getName(), targets);
			this.requiredEnergy = this.getRequiredEnergy(targets);
		}
		return CreepAction.targets.get(this.getName());
	}
	getName() {
		return this.constructor.name;
	}
	getDesiredCreepCount(creepCapacity) {
		let structures = this.getCachedTargets();
		let count = 0;
		if (structures.length) {
			let amount = this.requiredEnergy / creepCapacity;
			if (amount > 20) {
				count = 10;
			}
			else if (amount < 1)
				count = 1;
			else {
				count = Math.floor(amount);
			}
		}
		return count;
	}

	getMinCreeps() {
		return 0;
	}
	static getCreepActionName(creep) {
		return creep.memory['creepAction'];
	}
	static cleanup() {
		CreepAction.targets.clear();
		CreepAction.room = null;
	}
	canAssign() {
		return this.getCachedTargets() && this.getCachedTargets().length > 0;
	}
	assignCreep(creep) {
		let targets = this.getCachedTargets();
		this.assignedCount++;
		if (targets.length > 0) {
			let id;
			if (targets.length === 1) {
				id = targets[0].id;
			}
			else {
				id = creep.pos.findClosestByPath(targets).id;
			}
			creep.memory['creepAction'] = this.getName();
			creep.memory['targetId'] = id;
		}
	}
	static unassignCreep(creep) {
		creep.memory['creepAction'] = undefined;
		creep.memory['targetId'] = undefined;
		creep.say('😔');
	}
}
CreepAction.room = null;
CreepAction.targets = new Map();

// noinspection JSUnresolvedVariable
module.exports = CreepAction;
