"use strict";
const MyRoom = require('MyRoom');
let config = require("Config");

const rooms = new Map();
// noinspection JSUnresolvedVariable
module.exports.loop = function () {
	for (let roomName in Game.rooms) {
		if (!rooms.has(roomName)) {
			rooms.set(roomName, new MyRoom(roomName, config));
		}
		rooms.get(roomName).run();
	}
};
