"use strict";
const CreepAction = require('CreepAction');

class ActionBuilder extends CreepAction {
	constructor() {
		super();
		this.action = '🚧 build';
		this.creepType = 'TypeBunny';
		this.findType = FIND_CONSTRUCTION_SITES;
		this.findOptions = {};
	}
	getRequiredEnergy(targets) {
		let energy = 0;
		for (let target of targets) {
			let site = target;
			energy += site.progressTotal - site.progress;
		}
		return energy;
	}
	execute(creep) {
		let site = Game.getObjectById(creep.memory['targetId']);
		if (site === undefined) {
			CreepAction.unassignCreep(creep);
		}
		else {
			let err = creep.build(site);
			if (err === ERR_NOT_IN_RANGE) {
				creep.moveTo(site, { visualizePathStyle: { stroke: '#ffffff' } });
			}
			else if (err === ERR_INVALID_TARGET) {
				CreepAction.unassignCreep(creep);
			}
			else if (err) {
				console.log("Build error: " + err);
			}
		}
	}
}
// noinspection JSUnresolvedVariable
module.exports = ActionBuilder;
