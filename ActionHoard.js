"use strict";
const ActionSpawnPower = require('ActionSpawnPower');

class ActionHoard extends ActionSpawnPower {
	constructor() {
		super();
		this.action = '⚡ charge';
		this.creepType = 'TypeHoarder';
		this.findOptions  = { filter: (s) => s.structureType === STRUCTURE_STORAGE &&
				s.store.getFreeCapacity(RESOURCE_ENERGY) > 0 };
	}
}
// noinspection JSUnresolvedVariable
module.exports = ActionHoard;
