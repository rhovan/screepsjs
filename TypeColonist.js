"use strict";

const TypeBunny = require("TypeBunny");

class TypeColonist extends TypeBunny {
	constructor(minCreeps, bodyParts, killTtl = 0) {
		super(minCreeps, bodyParts, killTtl);
	}
	getType() {
		return this.constructor.name;
	}
	getDesiredCreepCount(_creepCapacity) {
		return 1;
	}
}

// noinspection JSUnresolvedVariable
module.exports = TypeHoarder;
