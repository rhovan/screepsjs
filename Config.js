"use strict";

const TypeBunny = require('TypeBunny');
const TypeHoarder = require('TypeHoarder');
const ActionHoard = require('ActionHoard');
const ActionSpawnPower = require('ActionSpawnPower');
const ActionExtensionPower = require('ActionExtensionPower');
const ActionTowerPower = require('ActionTowerPower');
const ActionBuilder = require('ActionBuilder');
const ActionRoadRepair = require('ActionRoadRepair');
const ActionWallRepair = require('ActionWallRepair');
const ActionUpgrader = require('ActionUpgrader');

const roomConfig = {
	'default': {
		L1: [
			new TypeBunny(15, [WORK, CARRY, MOVE]),
			new TypeHoarder(0, []),
		],
		L2: [
			new TypeBunny(15, [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE]),
			new TypeHoarder(0, []),
		],
		L3: [
			new TypeBunny(15, [WORK, WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE]),
			new TypeHoarder(0, []),
		],
		L5: [
			new TypeBunny(8, [WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE]),
			new TypeHoarder(0, []),
		],
		L6: [
			new TypeBunny(3, [
				WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,
				CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
				MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,
				MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,
			]),
			new TypeHoarder(1, [
				WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,  WORK,
				WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY,
				MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,  MOVE,
			], 50),

		]
	},
};
const actionSequence = [
	new ActionHoard(),
	new ActionSpawnPower(),
	new ActionExtensionPower(),
	new ActionTowerPower(),
	new ActionBuilder(),
	new ActionRoadRepair(),
	new ActionWallRepair(),
	new ActionUpgrader(),
];
class Config {
	constructor(roomLevels, actionSequence) {
		this.actions = new Map();
		this.roomLevels = new Map();
		this.importActions(actionSequence);
		this.importRoomLevels(roomLevels);
	}
	importActions(actionSequence) {
		for (let action of actionSequence) {
			if (!this.actions.has(action.getCreepType())) {
				this.actions.set(action.getCreepType(), new Map());
			}
			this.actions.get(action.getCreepType()).set(action.getName(), action);
		}
		console.log("Added", actionSequence.length, "Creep actions");
	}
	importRoomLevels(roomLevels) {
		for (let roomName in roomLevels) {
			let creepTypeLevelMap = new Map();
			for (let level in roomLevels[roomName]) {
				let creepTypeMap = new Map();
				for (let creepType of roomLevels[roomName][level]) {
					creepTypeMap.set(creepType.getType(), creepType);
				}
				creepTypeLevelMap.set(level, creepTypeMap);
			}
			this.roomLevels.set(roomName, creepTypeLevelMap);
		}
	}
	getRoomLevels(roomName) {
		let roomLevels;
		if (this.roomLevels.has(roomName)) {
			roomLevels = this.roomLevels.get(roomName);
		}
		else {
			roomLevels = this.roomLevels.get('default');
		}
		return roomLevels;
	}
}

// noinspection JSUnresolvedVariable
module.exports = new Config(roomConfig, actionSequence);

