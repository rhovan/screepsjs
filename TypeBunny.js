"use strict";
const CreepType = require('CreepType');
const CreepAction = require('CreepAction');

class TypeBunny extends CreepType {
	constructor(minCreeps, bodyParts, killTtl = 200) {
		super(minCreeps, bodyParts, killTtl);
		this.forceSource = false;
	}
	getType() {
		return "TypeBunny";
	}
	isCharging(creep) {
		return creep.memory['charging'] === true;
	}
	setCharging(creep) {
		creep.memory['charging'] = true;
		creep.memory['sourceId'] = undefined;
		creep.say('🔄 harvest');
	}
	setExecuting(creep) {
		creep.memory['charging'] = false;
		creep.memory['role'] = undefined;
		creep.memory['targetId'] = undefined;
		CreepAction.unassignCreep(creep);
	}
	checkChargeStatus(creep, energy) {
		let mayProceed = true;
		if (this.isCharging(creep) && creep.store.getFreeCapacity(RESOURCE_ENERGY) === 0) {
			energy.stopLeach(creep);
			this.setExecuting(creep);
		}
		else if (!creep.memory['charging'] && creep.store[RESOURCE_ENERGY] === 0) {
			if (creep.ticksToLive < this.killTtl) {
				delete Memory.creeps[creep.name];
				console.log("Creep", creep.name, "sacrificed itself for the greater good.");
				creep.suicide();
				mayProceed = false;
			}
			else {
				this.setCharging(creep);
			}
		}
		return mayProceed;
	}
	prepareForAction(creep, room) {
		let energy = room.getEnergyItems(creep, this.forceSource);
		let mayProceed = this.checkChargeStatus(creep, energy);
		if (mayProceed && this.isCharging(creep)) {
			if (!energy.isLeeching(creep)) {
				energy.startLeach(creep);
			}
			if (energy.isLeeching(creep)) {
				energy.leech(creep);
			}
		}
		return mayProceed && !this.isCharging(creep);
	}
}
// noinspection JSUnresolvedVariable
module.exports = TypeBunny;
