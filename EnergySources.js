"use strict";

const EnergyItems = require("EnergyItems");

class EnergySources extends EnergyItems {
	buildItems(room) {
		let sources = room.find(FIND_SOURCES);
		for (let source of sources) {
			this.items.set(source.id, 0);
		}
	}
	getEnergyCapacity(id) {
		return this.getItem(id).energy;
	}
	leechEnergy(item, creep) {
		return creep.harvest(item);
	}
}
// noinspection JSUnresolvedVariable
module.exports= EnergySources;
