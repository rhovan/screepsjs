"use strict";

class EnergyItems {
	buildItems() {}

	constructor(room) {
		this.items = new Map();
		this.buildItems(room);
	}

	hasStorage()
	{
		return this.items.size > 0;
	}

	getItem(id) {
		return Game.getObjectById(id);
	}

	isLeeching(creep) {
		return this.getItemId(creep) !== undefined;
	}

	getLeechType(creep) {
		return creep.memory['leechType'];
	}

	isLeechingFromMe(creep) {
		return this.getLeechType(creep) === this.constructor.name;
	}

	getItemId(creep) {
		return creep.memory['leechId'];
	}

	startLeach(creep) {
		let id = this.getMostAvailableItem();
		creep.memory['leechId'] = id;
		creep.memory['leechType'] = this.constructor.name;
		this.items.set(id, this.items.get(id) + 1);
	}

	leech(creep) {
		let item = Game.getObjectById(this.getItemId(creep));
		let err = this.leechEnergy(item, creep);
		if (err === ERR_NOT_IN_RANGE) {
			let err = creep.moveTo(item, { visualizePathStyle: { stroke: '#ffaa00' } });
			if (err === ERR_NO_PATH) {
				this.stopLeach(creep);
			}
			else if (err) {
				console.log("Move error: " + err);
			}
		}
		else if (err) {
			console.log(this.constructor.name, creep.memory['creepType'], err);
			this.stopLeach(creep);
		}
	}

	getMostAvailableItem() {
		let minCount = 999;
		let itemId;
		for (let id of this.items.keys()) {
			if (this.items.get(id) < minCount && this.getEnergyCapacity(id) > 0) {
				minCount = this.items.get(id);
				itemId = id;
			}
		}
		return itemId;
	}

	stopLeach(creep, ignoreItems = false) {
		let id = this.getItemId(creep);
		if (id !== undefined) {
			if (!ignoreItems) {
				this.items.set(id, Math.max(0, this.items.get(id) - 1));
			}
			creep.memory['leechId'] = undefined;
			creep.memory['leechType'] = undefined;
		}
	}
}

// noinspection JSUnresolvedVariable
module.exports = EnergyItems;
