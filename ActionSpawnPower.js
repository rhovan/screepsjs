"use strict";
const CreepAction = require('CreepAction');

class ActionSpawnPower extends CreepAction {
	constructor() {
		super();
		this.structureType = STRUCTURE_SPAWN;
		this.creepType = 'TypeBunny';
		this.action = '⚡ charge';
		this.findOptions = {
			filter: (structure) => (
					structure.structureType === STRUCTURE_SPAWN ||
					structure.structureType === STRUCTURE_EXTENSION ||
					structure.structureType === STRUCTURE_TOWER) &&
				structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
			};
		this.sort = s => s.structureType === STRUCTURE_TOWER ? 2 : 1

	}
	getMinCreeps() {
		return 0;
	}
	getRequiredEnergy(targets) {
		let energy = 0;
		for (let target of targets) {
			energy += target.store.getFreeCapacity(RESOURCE_ENERGY);
		}
		return energy;
	}
	execute(creep) {
		let target = Game.getObjectById(creep.memory['targetId']);
		if (target.store === undefined) {
			console.log(target, this.getName());
		}
		if (!target || target.store.getFreeCapacity(RESOURCE_ENERGY) === 0) {
			CreepAction.unassignCreep(creep);
		}
		else if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
			creep.moveTo(target, { visualizePathStyle: { stroke: '#ffffff' } });
		}
	}
}
// noinspection JSUnresolvedVariable
module.exports = ActionSpawnPower;
