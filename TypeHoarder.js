"use strict";

const TypeBunny = require("TypeBunny");

class TypeHoarder extends TypeBunny {
	constructor(minCreeps, bodyParts, killTtl = 200) {
		super(minCreeps, bodyParts, killTtl);
		this.forceSource = true;
	}
	getType() {
		return "TypeHoarder";
	}
	getDesiredCreepCount(_creepCapacity) {
		return 2;
	}
	prepareForAction(creep, room) {
		let ret = super.prepareForAction(creep, room);
		if (creep.pos.lookFor(LOOK_STRUCTURES).length === 0) {
			creep.pos.createConstructionSite(STRUCTURE_ROAD);
		}
		return ret;
	}
}

// noinspection JSUnresolvedVariable
module.exports = TypeHoarder;
