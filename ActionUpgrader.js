"use strict";
const CreepAction = require('CreepAction');

class ActionUpgrader extends CreepAction {
	constructor() {
		super();
		this.creepType = 'TypeBunny';
		this.action = '⚡ upgrade';
	}
	getMinCreeps() {
		return CreepAction.room.controller.ticksToDowngrade < 100000 ? 1 : 0;
	}
	getDesiredCreepCount(_creepCapacity) {
		return 999;
	}
	getTargets(room) {
		return [room.controller];
	}
	getRequiredEnergy(targets) {
		let control = targets[0];
		return control.progressTotal - control.progress;
	}
	execute(creep) {
		if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
			creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#ffffff' } });
		}
	}
}
// noinspection JSUnresolvedVariable
module.exports = ActionUpgrader;
